/*
	SPUDriver uses HE's SPU emulation to play ".spu" music files.

	".spu" files are audio RAM logs dumped by the "Eternal SPU" plugin. 

	It seems to be a "PlayStation 1" specific dump (no PS2 support - at least not here).
	
	Unfortunately the source code of "Eternal SPU" does not seem to be available. Nor does 
	there seem to be any specification of the file format or what exact behavior is expected to be 
	triggered by the "commands" recorded in these logs. 
	
	This impl is therefore guesswork based on reading Pete's "PSEmu Pro SPU plugin interface
	(http://www.pbernert.com/html/dev.htm), ancient japanese source code (see 
	https://web.archive.org/web/20051125182745/http://hp.vector.co.jp/authors/VA015058/source/kbspu_k_007s.cab ) 
	as well as the source code of "SPU log decoder" and "Audio Overload SDK". (Please let 
	me know if you have a proper format specification and/or "Eternal SPU Player 1.20" source code.)
		
	The problems with this poorly defined format already start with the "meta info" patched
	into the start of the dumps: Many Some files seem to use some japaneese Windows codetable
	but it is unclear if that is a coincidence or by design. The "Eternal SPU
	Player" from the creators of the format shows three info fields (game, title, copyright) but
	some songs contain more than three fields.. what is the maximum number of fields 
	and what is their meaning, etc?
	
	Note: ".spu" files seem to come in two flavors/formats. But the "magic" bytes sometimes 
	found at the beginning of the files ("SPU", "SPU1", "") CANNOT BE relied on for 
	identification! (The respective start-area is unused for playback purposes and it may 
	also contain just random garbage - see 07-gameover.spu)
		

	Copyright (c) 2023 Juergen Wothke
	
	
	License
	
	Out of necessity the same garbage GPL license applies here.
*/
#ifndef _SPU_DRIVER_H_
#define _SPU_DRIVER_H_

namespace psx {
	
class SPUDriver {
private:
	SPUDriver();
	
	// init
	int resetEnv();
	void processHeader(const uint8_t *buffer, uint32_t length);
	void loadDummyExe();
	void extractMetaInfo(const uint8_t *buffer);
	
	// header parsing
	void initFormatHandler(const uint8_t *buffer, uint32_t length);
	
	// handled commands
	void cmdWriteRegister(const uint8_t **buf_ptr);
	void cmdReadRegister(const uint8_t **buf_ptr);
	void cmdWriteDMA(const uint8_t **buf_ptr);
	void cmdReadDMA(const uint8_t **buf_ptr);
	void cmdPlayXA(const uint8_t **buf_ptr);
	void cmdPlayCDDA(const uint8_t **buf_ptr);

	
	void tick();
	void tickFormatOld();
	void tickFormatNew();
	uint32_t getEndOld(const uint8_t *cmdPtr);

	uint32_t calcTweakedTicks(uint8_t matchOp, uint32_t startTick, const uint8_t* buffer, uint32_t nsamples);
	void playBuffer(uint8_t matchOp, uint32_t startTick, const uint8_t *buffer, uint32_t srcSampleRate,  uint32_t srcSampleCount);

public:
	static SPUDriver *getInstance();
	
	int loadSong(const char *songmodule);
	
	int fillAudioBuffer(uint32_t sampleCount, int16_t *smplBuffer);

	int getMaxPos();
	int getCurrentPos();
	void setPos(int ms);

private:

};
	
}




#include <stdint.h>









































#endif