/*
 psx_adapter.js: Adapts HighlyExperimental backend to generic WebAudio/ScriptProcessor player.

   version 1.1
   copyright (C) 2015-2023 Juergen Wothke

 LICENSE

 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version. This library is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/
class PSXBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor(presetBIOS)
	{
		super(backend_PSX.Module, 2, new SimpleFileMapper(backend_PSX.Module));

//		this._manualSetupComplete = presetBIOS ? false : true;

		this.ensureReadyNotification();
	}
/*
	separate BIOS upload feature no longer implemented..
	
	uploadFile(filename, options)
	{
		if (options.setBIOS === 'undefined')
		{
			return -1;
		}
		else
		{
			// if not explicitly set here.. then 'emu_load_file' will go for some default later..

			if (this.Module.ccall('emu_set_bios', 'number', ['string'], [filename]) == 0)
			{
				this._manualSetupComplete = true;
				if (!(this._observer === 'undefined')) this._observer.notify();
				return 0;
			}
			else
			{
				return -1;
			}
		}
	}
*/
	loadMusicData(sampleRate, path, filename, data, options)
	{
		filename = this._getFilename(path, filename);

		let ret = this.Module.ccall('emu_load_file', 'number',
							['string', 'number', 'number', 'number', 'number', 'number'],
							[ filename, 0, 0, ScriptNodePlayer.getWebAudioSampleRate(), -999, false]);

		if (ret == 0)
		{
			this._setupOutputResampling(sampleRate);
		}
		return ret;
	}

	evalTrackOptions(options)
	{
		super.evalTrackOptions(options);

		let boostVolume= (options && options.boostVolume) ? options.boostVolume : 0;
		this.Module.ccall('emu_set_boost', 'number', ['number'], [boostVolume]);

		// no subsongs in PSX
		return 0;
	}

	getSongInfoMeta()
	{
		return {
			title: String,
			artist: String,
			game: String,
			year: String,
			genre: String,
			copyright: String,
			psfby: String
		};
	}

	decodeText(ptr)
	{
		return this._decodeBinaryToText(ptr, "ms_kanji", 256);	// used in SPU files
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		
		let numAttr = 7;
		let ret = this.Module.ccall('emu_get_track_info', 'number');
		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);

		result.title = this.decodeText(array[0]);
		if (!result.title.length) result.title = this._makeTitleFromPath(filename);

		result.artist = this.decodeText(array[1]);
		result.game = this.decodeText(array[2]);
		result.year = this.decodeText(array[3]);
		result.genre = this.decodeText(array[4]);
		result.copyright = this.decodeText(array[5]);
		result.psfby = this.decodeText(array[6]);
	}
};
