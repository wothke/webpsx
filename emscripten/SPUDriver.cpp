/*
	SPUDriver

	Copyright (c) 2023 Juergen Wothke

	License

	Out of necessity the same garbage GPL license applies here.
*/

// reminder: memory transfers to/from SPU mem are controlled via:
//         Sound RAM Data Transfer Address (0x1F801DA6) and
//         Sound RAM Data Transfer Fifo(0x1F801DA8)
// The current transfer address (see "tsa" in HE's spu impl) is initialized via 0x1F801DA6
// and it is then automatically incremented when word sized reads/writes are performed
// (regardless if triggered via 0x1F801DA8 or via DMA). See get_transfer/set_transfer


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "psx.h"
#include "iop.h"
#include "spu.h"
#include "r3000.h"

#include "MetaInfoHelper.h"

#include "SPUDriver.h"
#include "decode_xa.h"

using emsutil::MetaInfoHelper;


// --- interface to other webPSX parts
extern "C" int ems_request_file(const char *filename);		// callback.js (interface to async web world..)
extern "C" int he_install_bios(const char *he_bios_path);	// heplug.c
extern "C" size_t get_file_size( FILE * f);					// Adapter.cpp


uint32_t readU32(const unsigned char *p) {
	return p[0] | p[1]<<8 | p[2]<<16 | p[3]<<24;
}
uint16_t readU16(const unsigned char *p) {
	return p[0] | p[1]<<8;
}
int32_t readI32(const unsigned char *p) {
	unsigned int t= readU32(p);
	return *(int32_t*)&t;
}



namespace psx {

static SPUDriver *_instance = 0;

// since this is a singleton: I'll put the instance vars here - rather than
// clutter the API with these internal details (C++ is such garbage..)

static bool _running = true;

	// emulation state
static void * _emu = 0;
static void *_spu = 0;

	// standalone handling of XA play

static xa_decode_t _xaDecode;
static int32_t *_xaBuffer = 0; 	 		// input PCM buffer	 - stereo 16bit: L+R channel in same int
static float 	_xaPos, _xaStep = 0;	// poor man's resampling of the PCM butter to the used playback rate


	// .spu file handling
static const uint8_t*	_spuFileBuffer = 0;
static uint32_t		_spuFileBufferLen = 0;

static const uint8_t *_currentCmdPtr;
static uint32_t _currentTick, _nextTick, _endTick;
static uint32_t _currentEvent, _eventCount;
static bool 	_isOld;

static char _game[0x41], _song[0x41], _company[0x41], _ripby[0x41];


SPUDriver *SPUDriver::getInstance() {
	if (!_instance) {
		_instance= new SPUDriver();
	}
	return _instance;
}


SPUDriver::SPUDriver() {
}

void SPUDriver::initFormatHandler(const uint8_t *buffer, uint32_t length) {
	_isOld = readU32(&buffer[0x80200]) == 44100 ? 1 : 0;

	if (_isOld) {
		_eventCount = readU32(&buffer[0x80204]);

		if ((0x80208 + _eventCount * 12) > length) {
			_isOld = 0;
		}
		else {			
			_currentEvent = _currentTick = 0;
		}
	}

	if (!_isOld) {
		_endTick = readU32(&buffer[0x80200]);
		_currentTick = readU32(&buffer[0x80204]);
		_nextTick = _currentTick;
	} else {
		_endTick = getEndOld(&buffer[0x80208]);
	}

	_currentCmdPtr = &buffer[0x80208];
}

void SPUDriver::processHeader(const uint8_t *buffer, uint32_t length) {

	// upload the SPU RAM image
	uint16_t * pts = (uint16_t *)&buffer[0];	// at this point mem-alignment is still ok
	spu_sh( _spu, 0x1f801da6, 0 );
	for (int i=0; i < 0x40000; i++ ) {		// copy 0x80000 bytes to SPURAM
		spu_sh(_spu, 0x1f801da8, pts[i]);
	}

	// apply the register image..
	pts = (uint16_t *)&buffer[0x80000];
	for ( unsigned long reg = 0x1f801c00; reg <= 0x1f801dff; reg += 2 )	{
		// ignore DMA registers
		if ( reg == 0x1f801da8 ) continue;
		spu_sh( _spu, reg, pts[(reg - 0x1f801c00) >> 1] );
	}

	initFormatHandler(buffer, length);

	extractMetaInfo(buffer);

	_running = true;
}

int SPUDriver::loadSong(const char *songmodule) {
	int r = ems_request_file(songmodule);	// trigger load & check if ready
	if (r <0) {
		// should never happen since web page already loaded the song file, i.e.
		// it already is registered  in EMSCRIPTEN's file system

		return -1; // file not ready
	}

	FILE * f= fopen(songmodule, "r");
	_spuFileBufferLen = get_file_size( f );

	if (_spuFileBuffer) {
		free((void*)_spuFileBuffer);
	}
	_spuFileBuffer = (const uint8_t*)malloc( _spuFileBufferLen );

	fread((void*)_spuFileBuffer, 1, _spuFileBufferLen, f );

	fclose( f );

	if (resetEnv()) return -1;

	processHeader(_spuFileBuffer, _spuFileBufferLen);

	return 0;
}

int SPUDriver::resetEnv() {	// todo: strip down to SPU use only..

	int psxVersion = 1;		// only PS1 supported
	he_install_bios("");

    if(psx_init()) {
		fprintf(stderr, "error: psx_init()\n");
		return 1;
	}

	if (_emu) {
		free(_emu);
	}
	_emu = malloc( psx_get_state_size( psxVersion ));

	psx_clear_state( _emu, psxVersion );

	loadDummyExe();	// give the CPU something to run

	void* iop = psx_get_iop_state(_emu);
	_spu = iop_get_spu_state(iop);

	spu_clear_state( _spu, psxVersion );
	spu_enable_reverb( _spu, 1 );

	return 0;
}


void SPUDriver::loadDummyExe() {

	// endless loop (using relative jmps to ease relocation)

	//	00400000: <foo1> ; <input:0> foo1:
	//	00400000: 1480ffff ; <input:1> bne $a0,$zero,foo1
	//	00400004: 1080fffe ; <input:2> beq $a0,$zero,foo1
	unsigned char loopPrg[8] = { 0xff, 0xff, 0x80, 0x14, 0xfe, 0xff, 0x80, 0x10};

    uint32_t addr = 0x10000;
    uint32_t size = 8;

    void * pIOP = psx_get_iop_state( _emu );
    iop_upload_to_ram( pIOP, addr, &loopPrg[0], size );

	void * pR3000 = iop_get_r3000_state( pIOP );
	r3000_setreg(pR3000, R3000_REG_PC, addr );
}

void SPUDriver::extractMetaInfo(const uint8_t *buffer) {
	if (!strncmp((char *)buffer, "SPU", 3)) {
		strncpy(_game, (char *)&buffer[4], 0x40);
		strncpy(_song, (char *)&buffer[0x44], 0x40);
		strncpy(_company, (char *)&buffer[0x84], 0x40);
		strncpy(_ripby, (char *)&buffer[0xa4], 0x40);

		_game[0x40] = _song[0x40] = _company[0x40] = _ripby[0x40] = 0;	// just in case
	}
	else {
		_game[0x0] = _song[0x0] = _company[0x0] = _ripby[0x0] = 0;
	}

	MetaInfoHelper *info = MetaInfoHelper::getInstance();
	info->setText(0, _song, "");
	info->setText(1, _company, "");
	info->setText(2, _game, "");
	info->setText(6, _ripby, "");
}

uint32_t SPUDriver::getEndOld(const uint8_t *cmdPtr) {
	cmdPtr +=(_eventCount-1)*12;
	return readU32(&cmdPtr[0]);
}

void SPUDriver::tickFormatOld() {
	if (_currentEvent < _eventCount) {

		uint32_t ts = readU32(&_currentCmdPtr[0]);

		while ((ts == _currentTick) && (_currentEvent < _eventCount)) {
			uint32_t reg = readU32(&_currentCmdPtr[4]);
			uint16_t val = readU16(&_currentCmdPtr[8]);

			spu_sh(_spu, reg, val);

			_currentEvent++;
			_currentCmdPtr += 12;

			ts = readU32(&_currentCmdPtr[0]);
		}
	} else {
		_running = false;
	}
}

void SPUDriver::cmdWriteRegister(const uint8_t **buf_ptr) {

	uint32_t reg = readU32(&(*buf_ptr)[0]);
	uint16_t val = readU16(&(*buf_ptr)[4]);
//	fprintf(stderr, "WRITE %X %X\n", reg, val);

	spu_sh(_spu, reg, val);

	(*buf_ptr) += 6;
}

void SPUDriver::cmdReadRegister(const uint8_t **buf_ptr) {
	uint32_t reg = readU32(&(*buf_ptr)[0]);
//	fprintf(stderr, "READ %X\n", reg);

	uint16 d = spu_lh(_spu, reg);

	(*buf_ptr) += 4;
}

void SPUDriver::cmdWriteDMA(const uint8_t **buf_ptr) {
	// Pete's interface suggest that respective data is immediately
	// copied without any consideration of regular DMA latencies

	// testcase: 01-purple wind.spu fills soundbuffer to then be used as repeat address of some voice..

	uint32_t size = readU32(&(*buf_ptr)[0]) * 2;
	uint8_t* srcBuf = (uint8_t*)&(*buf_ptr)[4];

	// equivalent to using loop with spu_sh
	spu_dma( _spu, 0, srcBuf, 0, ~0, size, 1 );

	(*buf_ptr) += (4 + size);
}

void SPUDriver::cmdReadDMA(const uint8_t **buf_ptr) {

	uint32_t size = readU32(&(*buf_ptr)[0]);
	fprintf(stderr, "dma read %d!\n", size);	// needs a testcase!

	for(int i = 0; i<size; i++) {
		// if running a complete PSX emulation one might acually copy
		// the data over to the PSX mem.. but who would be using that?

		uint16 d = spu_lh(_spu, 0x1F801DA8); // Sound RAM Data Transfer Fifo
	}

	(*buf_ptr) += 4;
}

uint32_t SPUDriver::calcTweakedTicks(uint8_t matchOp, uint32_t startTick, const uint8_t* buffer, uint32_t nsamples) {

	// try to determine the length of the actual interval that needs to be
	// covered by the current block of samples - to compensate for the bad
	// timing precision of the recorded ticks (presuming this should be a
	// continuous xa stream - i.e. look ahead for the following xa command..)

	uint32_t tickMax = nsamples*2;

	uint32_t tick = readU32(buffer);
	buffer+= 4;

	tickMax += tick;

	while( (tick < _endTick) && (tick <= tickMax)) {

		uint8_t opcode = buffer[0];
		buffer+= 1;

		if (matchOp == opcode)	
			return tick - startTick;	// adjust playback rate to match available interval
		
		switch (opcode){
			case 0:			// write register
				buffer+= 6;
				break;
			case 1:			// read register
				buffer+= 4;
				break;
			case 2:			// write dma
				buffer+= (4 + readU32(&buffer[0]) * 2);	// len in shorts
				break;
			case 3:			// read dma
				buffer+= 4;
				break;
			case 4:			// play xa
				buffer += (32 + 32768);
				break;
			case 5: 		// play cdda
				buffer+= (4 + readU32(&buffer[0]));	// len in bytes
				break;
			default:
				return 0;
		}
		tick = readU32(buffer);
		buffer+= 4;
	}

	return nsamples;	// no follow-up XA command.. just use the correct length then
}

void SPUDriver::playBuffer(uint8_t matchOp, uint32_t startTick, const uint8_t *buffer, uint32_t srcSampleRate,  uint32_t srcSampleCount) {
	// precision of the file's "tick" timestamps is garbage and the intervals often are not in
	// sync with the amount of available sample data

	// number of samples that should be played if the correct fixed sample rate was used
	uint32_t nsamples = (uint32_t)round(((float)srcSampleCount) * 44100 / srcSampleRate);

	uint32_t tweakedTicks = calcTweakedTicks(matchOp, startTick, buffer, nsamples);

	_xaPos = 0;
	_xaStep = ((float)srcSampleCount)/ tweakedTicks;
}

void SPUDriver::cmdPlayXA(const uint8_t **buf_ptr) {
	
	_xaDecode.freq =		readI32(&(*buf_ptr)[0]);	// e.g. 37800
	_xaDecode.nbits =		readI32(&(*buf_ptr)[4]);	// e.g. 4
	_xaDecode.stereo =		readI32(&(*buf_ptr)[8]);	// e.g. 1
	_xaDecode.nsamples =	readI32(&(*buf_ptr)[12]);	// e.g. 2016 (this matches the used payload)
	_xaDecode.left.y0 = 	readI32(&(*buf_ptr)[16]);
	_xaDecode.left.y1 = 	readI32(&(*buf_ptr)[20]);
	_xaDecode.right.y0 = 	readI32(&(*buf_ptr)[24]);
	_xaDecode.right.y1 = 	readI32(&(*buf_ptr)[28]);

	// see https://github.com/Ninoh-FOX/PCSX4ALL for impl how that data would originally be 
	// produced using "xa_decode_sector()" and then played via "SPUplayADPCMchannel()". That impl
	// suggests that the "left"/"right" info is actually irrelevant for playback..
	
	// get a properly aligned buffer
	memcpy((void*)&_xaDecode.pcm, (const uint8_t *)&(*buf_ptr)[32], _xaDecode.nsamples*_xaDecode.nbits);
	_xaBuffer = (int32_t*)&_xaDecode.pcm;	// signal that there is data
	
	// the question is "what exactly is the xa_decode_sector(xa_decode_t *xdp, unsigned char *sectorp,
	// int is_first_sector) API supposed to do with this input?" .. the one example I found plays
	// happily by using the raw data as regular PCM sample data..

	(*buf_ptr) += (32 + 32768);

	playBuffer(4, _nextTick, (*buf_ptr), _xaDecode.freq, _xaDecode.nsamples);
}

void SPUDriver::cmdPlayCDDA(const uint8_t **buf_ptr) {
	fprintf(stderr, "untested cdda impl used!\n");	// FIXME find testcase for this

	// just reuse the xa impl
	uint32_t numBytes = readU32(&(*buf_ptr)[0]);
	_xaDecode.nsamples = numBytes >> 2;
			
	memcpy((void*)&_xaDecode.pcm, (const uint8_t *)&(*buf_ptr)[4], numBytes);
	_xaBuffer = (int32_t*)&_xaDecode.pcm;	// signal that there is data

	(*buf_ptr) += (4 + numBytes);
	
	playBuffer(5, _nextTick, (*buf_ptr), 37800, _xaDecode.nsamples);	// no idea if sample rate is correct
}

void SPUDriver::tickFormatNew() {

	if (_currentTick < _endTick) {
		while (_currentTick == _nextTick) {
			uint8_t opcode = _currentCmdPtr[0];
			_currentCmdPtr++;

			switch (opcode)	{
				case 0: cmdWriteRegister( &_currentCmdPtr );	break;
				case 1:	cmdReadRegister(&_currentCmdPtr );		break;
				case 2:	cmdWriteDMA( &_currentCmdPtr );			break;
				case 3:	cmdReadDMA( &_currentCmdPtr );			break;
				case 4:	cmdPlayXA( &_currentCmdPtr );			break;
				case 5:	cmdPlayCDDA( &_currentCmdPtr );			break;
				default:
					fprintf(stderr, "Unknown opcode %d\n", opcode);
					exit(-1);
			}
			_nextTick = readU32(&_currentCmdPtr[0]);
			_currentCmdPtr += 4;
		}
	}
	else {
		_running = false;
	}
}

void SPUDriver::tick() {

	if (_isOld) {
		tickFormatOld();
	}
	else {
		tickFormatNew();
	}
	_currentTick++;
}

int SPUDriver::fillAudioBuffer(uint32_t sampleCount, int16_t *smplBuffer) {
	if (_running) {

		for (int i = 0; i < sampleCount; i++) {
			tick();

			if (_running) {	// updated by tick()

				if (_xaBuffer ) {

					// I guess that songs that use this feature DO NOT use regular SPU voice output
					// simultaneously (todo: if not, then something like spu_render_ext() should
					// be used here to allow for regular mixing..)

					*((int*)smplBuffer) = ((int*)_xaBuffer)[(uint32_t)round(_xaPos)];	// just copy the 2 16bit samples

					_xaPos+= _xaStep;

					if (_xaPos > _xaDecode.nsamples) {
						_xaBuffer = 0;
					}
				} else {
					spu_render(_spu, smplBuffer, 1);
					/*
					so far it looks as if a full PSX emulation isn't necessary here..
					uint32 c= 1;
					if ( psx_execute( _emu, 0x7fffffff, smplBuffer, &c, 0 ) < 0 ) {
						fprintf(stderr, "execution error\n" );
						return -1;
					}
					*/
				}
			} else {
				return i << 2;	// return what we have so far
			}

			smplBuffer += 2;	// next stereo sample
		}
		return sampleCount << 2;	// buffer size in bytes (using stereo 16-bit here)
	}
	return 0;	// song ended
}

int SPUDriver::getMaxPos() {
	return _endTick * 10 / 441;
}

int SPUDriver::getCurrentPos() {
	return _currentTick * 10 / 441;
}

void SPUDriver::setPos(int ms) {
	int targetTick = ms * 441 / 10;
	
	if (targetTick < _currentTick)
	{
		resetEnv();
		processHeader(_spuFileBuffer, _spuFileBufferLen);
	}
	while (_currentTick < targetTick)
	{
		tick();
	}
}


}


