let songs = [
	"Playstation Sound Format/Makoto Tomozawa/Resident Evil - Complete Disc/01 main menu.minipsf",
	"Playstation Sound Format/Josh Mancell/Crash Bandicoot/14 papu papu.psf",
	"Playstation Sound Format/Mark Mothersbaugh/Crash Bandicoot 2 - Cortex Strikes Back/01 title screen.minipsf",
	"Playstation Sound Format/Mark Mothersbaugh/Crash Bandicoot 3 - Warped/02 warp room.minipsf",
	"Playstation Sound Format/Masami Ueda/Resident Evil 2/06 - prologue.minipsf",

	"SPU/- unknown/Jinguji Saburu - Tomoshibi Ga Kienumani/02-crystal sirens.spu",
	"SPU/- unknown/Jinguji Saburu - Tomoshibi Ga Kienumani/04-kinchou.spu",

	"SPU/- unknown/Gundam Battle Assault 2/02-select.spu",
	"SPU/- unknown/Gundam Battle Assault 2/07-gameover.spu",

	"SPU/- unknown/Pixy Garden/03.spu",
	"SPU/- unknown/Pixy Garden/13.spu",

	"SPU/- unknown/Depth - Original Groove/01-peace-a.spu",
	"SPU/- unknown/Dezaemon Plus/dezaesys01.spu",

	"SPU/- unknown/Raiden DX/raidendxnr-02.spu",
	];

class PSXDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 	{ return "webPSX";}
	getDisplaySubtitle() 	{ return "Highly Experimental music..";}
	getDisplayLine1() { return this.getSongInfo().title + (this.getSongInfo().artist.length>0 ? (" ("+this.getSongInfo().artist+")") : "");}
	getDisplayLine2() { return this.getSongInfo().copyright; }
	getDisplayLine3() { return ""; }
};

class PSXControls extends BasicControls {
	constructor(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, doOnDropFile, current)
	{
		super(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, doOnDropFile, current);
	}

	_addSong(filename)
	{
		if ((filename.indexOf(".psflib") == -1) && (filename.indexOf(".psf2lib") == -1))
		{
			this._someSongs.splice(this._current + 1, 0, filename);
		}
	}
}

class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new PSXBackendAdapter(false);
	//	this._backend.setProcessorBufSize(2048);

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					// drag&dropped temp files start with "/tmp/"
					someSong = 	someSong.startsWith("/tmp/") ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;
					return [someSong, {}];
				};

			this._playerWidget = new PSXControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new PSXDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x0010ff,0x00f0ff], 1, 0.1);

			this._playerWidget.playNextSong();
		});
	}
}