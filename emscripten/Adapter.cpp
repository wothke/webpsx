/*
* This file adapts "HighlyExperimental" to the interface expected by my generic JavaScript player..
*
* Copyright (C) 2015-2023 Juergen Wothke
*
* LICENSE
*
* This library is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or (at
* your option) any later version. This library is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>     /* malloc, free, rand */
#include <sys/stat.h>

#include <exception>
#include <iostream>
#include <fstream>

#include "psx.h"
#include "MetaInfoHelper.h"
#include "SPUDriver.h"

using emsutil::MetaInfoHelper;
using psx::SPUDriver;


extern "C" {
	// ---- interface to HE impl (see heplug.c)

	void he_set_volume_boost(unsigned char shift_left_n);

	struct DB_fileinfo_t;
	int he_init (struct DB_fileinfo_t *info, const char * uri);
	struct DB_fileinfo_t *he_open (uint32_t hints);
	int he_read (struct DB_fileinfo_t *info, short * samples, int size);
	int he_install_bios(const char *he_bios_path);

	int he_get_samples_to_play (DB_fileinfo_t *info);
 	int he_get_samples_played (DB_fileinfo_t *info);
	int he_seek_sample (DB_fileinfo_t *info, int sample);
	int he_get_sample_rate (DB_fileinfo_t *info);

	int psf_info_meta2(void * context, const char * name, const char * value) {
		MetaInfoHelper *info = MetaInfoHelper::getInstance();

		if ( !strcasecmp( name, "title" ) ) {
			info->setText(0, value, "");
		} else if ( !strcasecmp( name, "artist" ) ) {
			info->setText(1, value, "");
		} else if ( !strcasecmp( name, "game" ) ) {
			info->setText(2, value, "");
		} else if ( !strcasecmp( name, "year" ) ) {
			info->setText(3, value, "");
		} else if ( !strcasecmp( name, "genre" ) ) {
			info->setText(4, value, "");
		} else if ( !strcasecmp( name, "copyright" ) ) {
			info->setText(5, value, "");
		} else if ( !strcasecmp( name, "psfby" ) ) {
			info->setText(6, value, "");
		}
		return 0;
	}

	static DB_fileinfo_t* _he_song_info = 0;

	// ---- utility used by both of the above

	size_t get_file_size( FILE * f) {
		int fd= fileno(f);
		struct stat buf;
		fstat(fd, &buf);
		return buf.st_size;
	}

}

static SPUDriver *_spu = SPUDriver::getInstance();

#define CHANNELS 2
#define BYTES_PER_SAMPLE 2
#define SAMPLE_BUF_SIZE	1024

namespace psx {

	class Adapter {
	public:
		Adapter() : _useSpuMode(false), _samplesAvailable(0)
		{
		}

		int loadFile(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, unsigned char scopesEnabled)
		{
			// fixme: sampleRate, audioBufSize, scopesEnabled support not implemented

			// it would be more robust to check the file content: SPU_MAGIC= 0x53505500, PS1_MAGIC= 0x50534601,
			// PS2_MAGIC= 0x50534602 but for now I don't want to load the file here..

			const char *ext= get_filename_ext(filename);
			_useSpuMode = !strcmp(ext, "spu") || !strcmp(ext, "SPU");

			if (_useSpuMode)
			{
				return _spu->loadSong(filename);
			}
			else
			{
				if (!_he_song_info) _he_song_info= he_open(0);

				_filename = std::string(filename);
				return he_init(_he_song_info, filename);
			}
		}

		void teardown()
		{
			MetaInfoHelper::getInstance()->clear();
		}

		int getSampleRate()
		{
			return _useSpuMode ? 44100 : he_get_sample_rate(_he_song_info);
		}

		void setBoost(unsigned char boost)
		{
			if (!_useSpuMode)
				he_set_volume_boost(boost);
		}

		char* getAudioBuffer()
		{
			return (char*)_sampleBuffer;
		}

		long getAudioBufferLength()
		{
			return _samplesAvailable;
		}

		int genSamples()
		{
			int ret;
			if(_useSpuMode)
			{
				ret = _spu->fillAudioBuffer(SAMPLE_BUF_SIZE, _sampleBuffer);
			}
			else
			{
				ret = he_read (_he_song_info, _sampleBuffer, SAMPLE_BUF_SIZE);	// returns number of bytes
			}

			if (ret < 0)
			{
				_samplesAvailable= 0;
				return 1;		// end song
			}
			else
			{
				_samplesAvailable= ret >>2; // 2*2bytes sample
				return _samplesAvailable ? 0 : 1;
			}
		}

		int getCurrentPosition()
		{
			if (_useSpuMode)
			{
				return _spu->getCurrentPos();
			}
			else
			{
				return ((long long)he_get_samples_played(_he_song_info)) * 1000 / he_get_sample_rate(_he_song_info);
			}
		}

		int getMaxPosition()
		{
			if (_useSpuMode)
			{
				return _spu->getMaxPos();
			}
			else
			{
				return ((long long)he_get_samples_to_play(_he_song_info)) * 1000 / he_get_sample_rate(_he_song_info);
			}
		}

		void seekPosition(int ms)
		{
			if (_useSpuMode)
			{
				_spu->setPos(ms);
			}
			else
			{
				int pos =  ((long long)ms) *  he_get_sample_rate(_he_song_info) / 1000;
				if (pos < he_get_samples_played(_he_song_info))
				{
					// he_seek_sample cannot seek backwards.. have to reload the song
					he_init(_he_song_info, _filename.c_str());
				}
				he_seek_sample(_he_song_info, pos);
			}
		}

	private:
		const char *get_filename_ext(const char *filename)
		{
			const char *dot = strrchr(filename, '.');
			if(!dot || dot == filename) return "";
			return dot + 1;
		}

	private:
		// see Sound::Sample::CHANNELS
		sint16 _sampleBuffer[SAMPLE_BUF_SIZE * CHANNELS];
		int _samplesAvailable;

		bool _useSpuMode;

		std::string _filename;
	};
}

static psx::Adapter _adapter;


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func


EMBIND(int, emu_load_file(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown()) 									{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate()) 								{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int subsong)) 						{ return 0; }
EMBIND(const char**, emu_get_track_info()) 						{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(char*, emu_get_audio_buffer()) 							{ return _adapter.getAudioBuffer(); }
EMBIND(long, emu_get_audio_buffer_length()) 					{ return _adapter.getAudioBufferLength(); }
EMBIND(int, emu_compute_audio_samples()) 						{ return _adapter.genSamples(); }
EMBIND(int, emu_get_current_position()) 						{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms)) 						{ _adapter.seekPosition(ms); }
EMBIND(int, emu_get_max_position()) 							{ return _adapter.getMaxPosition(); }

// --- add-ons
EMBIND(int, emu_set_bios(char *bios_path)) 						{ return he_install_bios(bios_path); } // the first installed BIOS stays; if "built-in hebios" is activated  bios_path is ignored
EMBIND(void, emu_set_boost(unsigned char boost)) 				{ return _adapter.setBoost(boost); }
